<?php namespace 

App\Http\Controllers; 
use Illuminate\Http\Request; 
use App\Uploaddata; 
use Mail; 

class DataController extends Controller { 

      public function saveData(Request $request) { 

        $this->validate($request, [
            'firstname' => 'required',
			'lastname' => 'required',
            'email' => 'required|email'
        ]);

		$upload = Uploaddata::create();
        $upload->firstname =  $request->get('firstname');
        $upload->lastname = $request->get('lastname');
        $upload->email = $request->get('email');
        $upload->filename = $request->get('image-upload');
        $upload->save();
		
		/*
		$data = ['name' => $request->get('firstname').' '.$request->get('lastname') , 'email' => $request->get('email'), 'from' => 'test@aufgabe.com' ];
		
        Mail::send('from', $data, function($message) {
            $message->to('email', 'name')
                    ->subject('Your upload was successful');
        });
 
        if (Mail::failures()) {
			return response()->Fail('Sorry! Please try again latter');
		}else{
			return response()->success('Great! Successfully send in your mail');
		}*/

		try {
		   mail($request->get('email'),"Your upload was successful","Thanks for your upload");
		   $success = true;
		} catch (Exception $e) {
		   echo $e->getMessage();
		}
        
        return view('dropzone-view',['success' => $success]);

    }
}