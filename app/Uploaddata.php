<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uploaddata extends Model
{
        protected $fillable = [
        'firstname',
        'lastname',
        'email',
		'filename'
    ];
}
