<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploaddatas', function (Blueprint $table) {
            $table->increments('id');
			$table->string('firstname', 255)->nullable();
            $table->string('lastname', 255)->nullable();
			$table->string('email')->nullable();
			$table->string('filename', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_datas');
    }
}
