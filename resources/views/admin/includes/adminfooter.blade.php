<footer class="section footer-classic context-dark bg-image footer2hm">
	<div class="container">
	  <div class="row row-30">
		  <div class="pr-xl-4"><a class="brand" href="index.html"><img class="brand-logo-light" src="https://www.2hm-bs.com/app/uploads/2017/05/logo_2hmbs_small-1.png" alt="" width="140" height="37" srcset="images/agency/logo-retina-inverse-280x74.png 2x"></a>
			<p class="rights"><span>©  </span><span class="copyright-year">2020</span><span> </span><span>Waves</span><span>. </span><span>All Rights Reserved.</span></p>
		  </div>
</footer>