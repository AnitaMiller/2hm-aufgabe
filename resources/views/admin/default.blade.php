
<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body>
<div class="fullpage">

    <header class="row">
        @include('admin.includes.adminheader')
    </header>

    <div id="main" class="row">

            @yield('content')

    </div>

    <footer class="row">
        @include('admin.includes.adminfooter')
    </footer>

</div>
</body>
</html>