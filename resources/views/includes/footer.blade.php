<footer class="section footer-classic context-dark bg-image footer2hm">
	<div class="container">
	  <div class="row row-30">
		<div class="col-md-4 col-xl-5">
		  <div class="pr-xl-4"><a class="brand" href="index.html"><img class="brand-logo-light" src="https://www.2hm-bs.com/app/uploads/2017/05/logo_2hmbs_small-1.png" alt="" width="140" height="37" srcset="images/agency/logo-retina-inverse-280x74.png 2x"></a>
			<p>WIR SCHAFFEN KUNDENDIALOGE UND KUNDENBEZIEHUNGEN DURCH DATENBASIERTES MARKETING</p>
			<!-- Rights-->
			<p class="rights"><span>©  </span><span class="copyright-year">2020</span><span> </span><span>Waves</span><span>. </span><span>All Rights Reserved.</span></p>
		  </div>
		</div>
		<div class="col-md-4">
		  <h5>Contacts</h5>
		  <dl class="contact-list">
			<dt>Address:</dt>
			<dd>Emmerich-Josef-Str. 5, Mainz</dd>
		  </dl>
		  <dl class="contact-list">
			<dt>email:</dt>
			<dd><a href="mailto:info@2hm-bs.com">info@2hm-bs.com</a></dd>
		  </dl>
		  <dl class="contact-list">
			<dt>phones:</dt>
			<dd>+49-6131-14 37 168
			</dd>
		  </dl>
		</div>
		<div class="col-md-4 col-xl-3">
		  <h5>Links</h5>
		  <ul class="nav-list">
			<li><a href="#">About</a></li>
			<li><a href="#">Projects</a></li>
			<li><a href="#">Blog</a></li>
			<li><a href="#">Contacts</a></li>
			<li><a href="#">Pricing</a></li>
		  </ul>
		</div>
	  </div>
	</div>
	<div class="row no-gutters social-container">
	  <div class="col"><a class="social-inner" href="#"><span class="icon mdi mdi-facebook"></span><span>Facebook</span></a></div>
	  <div class="col"><a class="social-inner" href="#"><span class="icon mdi mdi-instagram"></span><span>instagram</span></a></div>
	  <div class="col"><a class="social-inner" href="#"><span class="icon mdi mdi-twitter"></span><span>twitter</span></a></div>
	  <div class="col"><a class="social-inner" href="#"><span class="icon mdi mdi-youtube-play"></span><span>google</span></a></div>
	</div>
</footer>