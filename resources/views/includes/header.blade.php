<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item active">
      <a class="nav-link" href="#">Home</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Über uns</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Kontakt</a>
    </li>
    <li class="nav-item">
      <a class="nav-link disabled" href="#">Team</a>
    </li>
  </ul>
</nav>