@extends('admin.default')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-striped">
						<thead>
							<tr>
							  <td>ID</td>
							  <td>Name</td>
							  <td>Email</td>
							  <td>Date</td>
							  <td>File</td>
							</tr>
						</thead>
						<tbody>
							@foreach($uploaddata as $data)
							<tr>
								<td>{{$data->id}}</td>
								<td>{{$data->firstname}} {{$data->lastname}}</td>
								<td>{{$data->email}}</td>
								<td>{{$data->created_at->format("d/m/Y")}} </td>
								<td>{{$data->filename}}</td>
							</tr>
							@endforeach
						</tbody>
					  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
