<!DOCTYPE html>
<html>
<head>
    @include('includes.head')
</head>
<body>

<header class="row">
	@include('includes.header')
</header>
<div class="container">

    <div class="row">
        <div class="col-md-12">
		<?php $msg = (isset($success) && $success == true) ? 'Ihre daten wurden erfolgreich versandt' : '';?>
			<div class="success"><?php echo $msg;?></div>
            {!! Form::open([ 'route' => [ 'dropzone.store' ], 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'image-upload' ]) !!}
            {!! Form::close() !!}
			
			<form method="post" action="save_data">
             {{csrf_field()}}
             <div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label> Vorname </label>
						<input type="text" class="form-control @error('firstname') is-invalid @enderror" placeholder="Vorname" name="firstname">
						@error('firstname')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
               </div>
			   <div class="col-md-12">
                 <div class="form-group">
                   <label> Nachname </label>
                   <input type="text" class="form-control @error('lastname') is-invalid @enderror" placeholder="Nachname" name="lastname">
                   @error('lastname')
                       <span class="invalid-feedback" role="alert">
                           <strong>{{ $message }}</strong>
                       </span>
                   @enderror
                 </div>
               </div>
             <div class="col-md-12">
               <div class="form-group">
                   <label> Email </label>
                   <input type="text" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email">
                   @error('email')
                       <span class="invalid-feedback" role="alert">
                           <strong>{{ $message }}</strong>
                       </span>
                   @enderror
                 </div>
               </div>   
            
             </div>
             <div class="row">
              <div class="update ml-auto mr-auto">
                 <button type="submit" class="btn btn-primary btn-round">Send</button>
               </div>
             </div>
           </form>
        </div>
    </div>
</div>

<footer class="row">
	@include('includes.footer')
</footer>


<script type="text/javascript">
        Dropzone.options.imageUpload = {
            maxFilesize         :       10,
			addRemoveLinks: true,
            acceptedFiles: ".jpeg,.jpg,.png,.pdf"
        };
</script>


</body>
</html>